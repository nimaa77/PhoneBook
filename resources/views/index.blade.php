<!doctype html>
<!--suppress ALL -->
<html lang="en" onload="">
<head>
    <meta charset="UTF-8">
    <title>PhoneBook</title>

    <!--Import Google Icon Font-->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="<?php echo url('/') ?>/css/materialize.min.css"  media="screen,projection"/>
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <link type="text/css" rel="stylesheet" href="<?php echo url('/') ?>/css/main.css"  media="screen,projection"/>

</head>
<body>
<header>
    <nav>
        <div class="nav-wrapper">
            <a href="#" class="brand-logo center">PhoneBook</a>
        </div>
    </nav>
</header>

<main>
    <div id="div1"></div>
    <div class="row">
        <div class="col m4 offset-m4">
            <div class="input-field col s12">
                <i class="material-icons prefix">search</i>
                <input id="search_text" type="text" class="validate">
            </div>
        </div>
        <div class="col s10 offset-s1">
            <table class="highlight">
                <thead>
                    <tr>
                        <th data-field="name">Name</th>
                        <th data-field="phone">Telephone</th>
                        <th data-field="phone">Mobile</th>
                        <th data-field="address">Address</th>
                        <th data-field="delete">Delete</th>
                        <th data-field="edit">Edit</th>
                    </tr>
                </thead>
                <tbody id="tbody">

               </tbody>
            </table>
            <!-- Modal Structure -->
            <div id="modalNew" class="modal bottom-sheet">
                <div class="modal-content">
                    <div class="row">
                        <form class="col s12">
                            <div class="row">
                                <div class="col l6 s12">
                                    <div class="input-field col s12">
                                        <i class="material-icons prefix">account_circle</i>
                                        <input id="first_name" type="text" class="validate">
                                        <label for="first_name"> Name</label>
                                    </div>
                                    <div class="input-field col s12">
                                        <i class="material-icons prefix">phone</i>
                                        <input id="phone" type="tel" class="validate">
                                        <label for="phone">Telephone</label>
                                    </div>
                                    <div class="input-field col s12">
                                        <i class="material-icons prefix">phone</i>
                                        <input id="mobile" type="tel" class="validate">
                                        <label for="mobile">Mobile Phone</label>
                                    </div>
                                </div>
                                <div class="col l6 s12">
                                    <div class="input-field col s12">
                                        <textarea id="address" class="materialize-textarea"></textarea>
                                        <label for="address">Address</label>
                                    </div>
                                    <a onclick="newContact()" href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat right">Add</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <!-- Modal Structure -->
            <div id="modalEdit" class="modal bottom-sheet">
                <div class="modal-content">
                    <div class="row">
                        <form class="col s12">
                            <div class="row">
                                <div class="col l6 s12">
                                    <div class="input-field col s12">
                                        <p id="edit_id" style="display: none;"></p>
                                        <i class="material-icons prefix">account_circle</i>
                                        <input id="edit_first_name" type="text" class="validate">
                                        <label for="edit_first_name"> Name</label>
                                    </div>
                                    <div class="input-field col s12">
                                        <i class="material-icons prefix">phone</i>
                                        <input id="edit_phone" type="tel" class="validate">
                                        <label for="edit_phone">Telephone</label>
                                    </div>
                                    <div class="input-field col s12">
                                        <i class="material-icons prefix">phone</i>
                                        <input id="edit_mobile" type="tel" class="validate">
                                        <label for="edit_mobile">Mobile Phone</label>
                                    </div>
                                </div>
                                <div class="col l6 s12">
                                    <div class="input-field col s12">
                                        <textarea id="edit_address" class="materialize-textarea"></textarea>
                                        <label for="edit_address">Address</label>
                                    </div>
                                    <a onclick="editContact()" href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat right">Edit</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <!-- Modal Structure -->
            <div id="modalDelete" class="modal">
                <div class="modal-content">
                    <h4>Delete</h4>
                    <p>Are Your Sure You Want to, Delete This User?</p>
                    <p>This Can not Be Undone.</p>
                    <p id="deleteID" style="display: none;"></p>
                </div>
                <div class="modal-footer">
                    <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">No</a>
                    <a onclick="removeContact()" href="#!" class=" modal-action modal-close waves-effect waves-red btn-flat">Yes</a>
                </div>
            </div>
        </div>
    </div>
    <div class="fixed-action-btn">
        <a  onclick="add()" class="btn-floating btn-large blue">
            <i class="large material-icons">add</i>
        </a>
    </div>
</main>

<footer class="page-footer">
    <div class="footer-copyright">
        <div class="container">
            © 2016 Copyright Nima Arefi
        </div>
    </div>
</footer>

</body>

    <!--Import jQuery before materialize.js-->
    <script type="text/javascript" src="<?php echo url('/') ?>/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo url('/') ?>/js/materialize.min.js"></script>

<script>
    base_url = "<?php echo url('/');?>";
</script>

<script type="text/javascript" src="<?php echo url('/') ?>/js/main.js"></script>


</html>