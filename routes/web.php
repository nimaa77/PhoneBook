<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MainController@index');
Route::get('/all', 'MainController@showAll');
Route::get('/add/{name}/{telephone}/{mobile}/{address}', 'MainController@add');
Route::get('/delete/{id}', 'MainController@delete');
Route::get('/update/{id}/{name}/{telephone}/{mobile}/{address}', 'MainController@update');
Route::get('/find/{pattern}/{filed}', 'MainController@find');
Route::get('/find/{filed}', 'MainController@findAll');
