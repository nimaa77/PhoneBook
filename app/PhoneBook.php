<?php
/**
 * Created by PhpStorm.
 * User: nima
 * Date: 11/23/16
 * Time: 11:23 AM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class PhoneBook extends Model
{
    protected  $fillable = ['id', 'name' , 'telephone' , 'mobilephone' , 'address'];

    protected $table = 'Book';

}