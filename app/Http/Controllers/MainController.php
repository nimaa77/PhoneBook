<?php
/**
 * Created by PhpStorm.
 * User: nima
 * Date: 11/23/16
 * Time: 12:39 AM
 */

namespace App\Http\Controllers;

use App\PhoneBook;


class MainController extends Controller
{

    public function headerSet() {

        header('Access-Control-Allow-Origin: *');
        header('Cache-Control: no-cache, must-revalidate');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Content-type: application/json; charset=utf-8');

    }

    public function index(){

        return view('index');

    }

    public function showAll() {

        $this->headerSet();

        return PhoneBook::all();

    }

    public function add($name, $telephone, $mobile, $address){

        $this->headerSet();

        $result = PhoneBook::create(array('name' => $name, 'telephone' => $telephone, 'mobilephone' => $mobile, 'address' => $address));

        if ($result)
            return array('success' => true);
        else
            return array('success' => false);
    }

    public function delete($id){

        $this->headerSet();

        $res = PhoneBook::destroy($id);

        return array('success' => $res == 1);
    }

    public function update($id, $name, $telephone, $mobile, $address){

        $this->headerSet();

        $res = PhoneBook::find($id);

        $result = $res->update(array('name' => $name, 'telephone' => $telephone, 'mobilephone' => $mobile, 'address' => $address));

        if ($result)
            return array('success' => true);
        else
            return array('success' => false);

    }

    public function find($inside, $data){

        $this->headerSet();

        return  PhoneBook::where($inside , 'LIKE' , '%'.$data.'%')->get();
    }

    public function findAll($data){

        $this->headerSet();

            return PhoneBook::where('name' , 'LIKE' , '%'.$data.'%')
                ->orWhere('telephone' , 'LIKE' , '%'.$data.'%')
                ->orWhere('mobilephone' , 'LIKE' , '%'.$data.'%')
                ->orWhere('address' , 'LIKE' , '%'.$data.'%')
                ->get();

    }

}