/**
 * Created by nima on 12/17/16.
 */

$( document ).ready(function() {

    $('.modal').modal();

    fetchData();

    $('#search_text').change(function () {

        if ($(this).val() == "")
            fetchData();

        $('#tbody').html("");

        searchAll($(this).val());

    });

});

function add() {

    $('#modalNew').modal('open');
}

function edit(id) {

    $.getJSON(base_url + "/find/id/" + id, function (result) {

        $.each(result, function(i, field) {

            $('#edit_first_name').val(field.name);
            $('#edit_phone').val(field.telephone);
            $('#edit_mobile').val(field.mobilephone);
            $('#edit_address').val(field.address);
            $('#edit_id').val(field.id);

            Materialize.updateTextFields();

        });

    });

    $('#modalEdit').modal('open');
}

function remove(id) {

    $('#deleteID').val(id);
    $('#modalDelete').modal('open');

}

function editContact() {

    $.getJSON(base_url + "/update/" + $('#edit_id').val() + "/" + $('#edit_first_name').val() + "/" + $('#edit_phone').val() + "/" + $('#edit_mobile').val() + "/" + $('#edit_address').val(), function (result) {

        if (result.success){

            $('#tbody').html("");
            fetchData();

        }

        else
            alert('Try Again');

    });

}

function removeContact() {

    $.getJSON(base_url + "/delete/" + $('#deleteID').val(), function (result) {

        if (result.success){

            $('#tbody').html("");
            fetchData();

        }

        else
            alert('Try Again');

    });
}

function newContact() {

    $.getJSON(base_url + "/add/" + $('#first_name').val() + "/" + $('#phone').val() + "/" + $('#mobile').val() + "/" + $('#address').val(), function (result) {

        if (result.success){

            $('#tbody').html("");
            fetchData();

            $('#first_name').val("");
            $('#phone').val("");
            $('#mobile').val("");
            $('#address').val("");


        }

        else
            alert('Try Again');

    });

}

function searchAll(query) {

    $.getJSON(base_url + "/find/" + query, function (result) {

        $.each(result, function(i, field){

            tableRow(field.id, field.name, field.telephone, field.mobilephone, field.address)

        });

    });

}

function tableRow(id, name, telephone, mobile, address) {

    str_exp = "<tr>";


    str_exp += "<td>" + name + "</td>";
    str_exp += "<td>" + telephone + "</td>";
    str_exp += "<td>" + mobile + "</td>";
    str_exp += "<td>" + address + "</td>";
    str_exp += '<td onclick="remove(' + id + ')"><i class="material-icons" style="color: #e53935;">delete</i></td>';
    str_exp += '<td onclick="edit(' + id + ')"><i class="material-icons" style="color: #00c853;">mode_edit</i></td>';

    str_exp += "</tr>";


    $('#tbody').append(str_exp);
}

function fetchData() {

    $.getJSON(base_url + "/all", function (result) {

        $.each(result, function(i, field){

            tableRow(field.id ,field.name, field.telephone, field.mobilephone, field.address);

        });

    });

}